# TypeScript Design Patterns
https://en.wikipedia.org/wiki/Software_design_pattern

## Klasyfikacja
Kreacyjne – Creational
Opisują proces tworzenia nowych obiektów; ich zadaniem jest tworzenie, inicjalizacja oraz konfiguracja obiektów, klas
oraz innych typów danych.

Strukturalne - Structural
Opisują struktury powiązanych ze sobą obiektów.

Czynnościowe / operacyjne - Behavioral
Opisują zachowanie i odpowiedzialność współpracujących ze sobą obiektów.

## Facade - Fasada (S)
Służy do ujednolicenia dostępu do złożonego systemu poprzez wystawienie uproszczonego, uporządkowanego interfejsu
programistycznego, który ułatwia jego użycie.

## Getter / Setter
Disclaimer
To nie jest wzorzec projektowy, lecz mechanizm języka programowania.
Warto poruszyć ten temat, bo zauważyłem, że sporo osób ma z tym problem.

## Template Method - Metoda szablonowa (B)
Jego zadaniem jest zdefiniowanie metody, będącej szkieletem algorytmu. Algorytm ten może być następnie dokładnie
definiowany w klasach pochodnych.

## Dependency Injection – Wstrzyknięcie zalezności (S)
Polega na przekazywaniu gotowych, utworzonych instancji obiektów udostępniających swoje metody i właściwości obiektom,
które z nich korzystają (np. jako parametry konstruktora). Stanowi alternatywę do podejścia, gdzie obiekty tworzą
instancję obiektów, z których korzystają np. we własnym konstruktorze.

Disclaimer
Odwrócenie sterowania (ang. Inversion of Control, IoC) – paradygmat (czasami rozważany też jako wzorzec projektowy lub
wzorzec architektury) polegający na przeniesieniu funkcji sterowania wykonywaniem programu do używanego frameworku.
Framework w odpowiednich momentach wywołuje kod programu stworzony przez programistę w ramach implementacji danej
aplikacji.
Termin ten jest najczęściej utożsamiany z wstrzykiwaniem zależności. Tymczasem wstrzykiwanie zależności jest tylko
jednym z przykładów realizacji IoC (w tym przypadku sterowanie zostaje odwrócone w obszarze tworzenia powiązań pomiędzy
obiektami). Termin „Wstrzykiwanie zależności” został wymyślony w celu rozróżnienia pomiędzy tymi dwoma terminami. Wiele
osób wskazuje, że formami IoC są również programowanie aspektowe oraz wzorzec strategii.

## Singleton (C)
Jego celem jest ograniczenie możliwości tworzenia obiektów danej klasy do jednej instancji oraz zapewnienie globalnego
dostępu do stworzonego obiektu.

## Startegy - Strategia (B)
Definiuje rodzinę wymiennych algorytmów i kapsułkuje je w postaci klas. Umożliwia wymienne stosowanie każdego z nich w
trakcie działania aplikacji niezależnie od korzystających z nich użytkowników.

## Factory Method - Metoda wytwórcza (C)
Jego celem jest dostarczenie interfejsu do tworzenia obiektów nieokreślonych jako powiązanych typów.

## Abstract Factory - Fabryka abstrakcyjna (C)
ToDo

## Startegy + Factory

## Lazy Initialization - Leniwe iniocjowanie (C)
Polega na opóźnianiu tworzenia obiektu, obliczania wartości lub przeprowadzania innych kosztownych operacji, aż do
momentu pierwszego zapotrzebowania na nie.

## Singleton + Factory + Lazy initialization = Lazy Factory

## Observer - Obserwator (B)
Używany jest do powiadamiania zainteresowanych obiektów o zmianie stanu pewnego innego obiektu.

## Decorator - Dekorator (S)
Pozwala na dodanie nowej funkcji do istniejących klas dynamicznie podczas działania programu.

## State - Stan (B)
Umożliwia zmianę zachowania obiektu poprzez zmianę jego stanu wewnętrznego. Innymi słowy – uzależnia sposób działania
obiektu od stanu w jakim się aktualnie znajduje.

## Builder - Budowniczy (C)
Jego celem jest rozdzielenie sposobu tworzenia obiektów od ich reprezentacji.
Disclaimer
Mało przydatny na forntendzie, możemy przekazac obiekt do konstruktora np. contructor(options = { ... }) { ... }