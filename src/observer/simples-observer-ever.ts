export class Subject {
    private _subscribers: Function[] = [];

    public register(o: Function): void {
        this._subscribers.push(o);
    }

    public unregister(o: Function): void {
        this._subscribers = this._subscribers.filter(s => s !== o);
    }

    public notify(data: any): void {
        this._subscribers.forEach(s => s(data));
    }
}

let s = new Subject();
let o1 = d => console.log(`Observer 1 says ${d}`);
let o2 = d => console.log(`Observer 2 says ${d}`);
let o3 = d => console.log(`Observer 3 says ${d}`);

s.register(o1);
s.register(o2);
s.register(o3);
s.notify('Hello!');

s.unregister(o1);

s.notify('Hello again!');

