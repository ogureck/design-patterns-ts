import * as Rx from 'rxjs';


let source = Rx.Observable.range(0, 3)
    .do(v => console.log('Do Next: ' + v));

let subscription1 = source.subscribe();



let subject = new Rx.BehaviorSubject<Date>(new Date());

let subscription2 = subject.subscribe(v => console.log(v));

setInterval(() => subject.next(new Date()), 500);
