import { newGuid } from '../helpers/newGuid';

// singleton + factory + lazy loading = lazy-factory

export class Fruit {
    private static readonly instances: { [key: string]: Fruit } = {};
    private readonly guid: string;

    private constructor(private type: string) {
        this.guid = newGuid();
    }

    public static getFruit(type: string): Fruit { // factory method
        let f: Fruit = Fruit.instances[type];

        if (!f) { // lazy initialization
            f = new Fruit(type);
            Fruit.instances[type] = f; // singleton
        }

        return f;
    }

    public toString(): string {
        return `This is ${this.type}, unique id ${this.guid}`;
    }
}

let basket: Fruit[] = [
    Fruit.getFruit('orange'),
    Fruit.getFruit('orange'),
    Fruit.getFruit('orange'),

    Fruit.getFruit('apple'),
    Fruit.getFruit('apple'),
    Fruit.getFruit('apple'),

    Fruit.getFruit('grape'),
    Fruit.getFruit('grape'),
    Fruit.getFruit('grape')
];
basket.forEach(f => console.log(f.toString()));

