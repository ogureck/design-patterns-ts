import { IProduct, OptionDecorator } from './base';

export class BasicShipment extends OptionDecorator {
    constructor(version: IProduct) {
        super(version, 9, 'Wysyłka Pocztą Polską');
    }
}

export class FastShipment extends OptionDecorator {
    constructor(version: IProduct) {
        super(version, 20, 'Wysyłka kurierem');
    }
}

export class InternationalShipment extends OptionDecorator {
    constructor(version: IProduct) {
        super(version, 50, 'Wysyłka zagraniczna');
    }
}

export class PresentWrapping extends OptionDecorator {
    constructor(version: IProduct) {
        super(version, 10, 'Opakowanie prezentowe');
    }
}

export class MemberDiscount extends OptionDecorator {
    constructor(version: IProduct) {
        super(version, 15, 'Zniżka klubowicza');
    }

    public getPrice(): number {
        return this.product.getPrice() * ((100 - this.price) / 100);
    }
}

export class AdditionalLaces extends OptionDecorator {
    constructor(version: IProduct) {
        super(version, 15, 'Dodatkowe sznurówki');
    }
}

export class AdditionalSponge extends OptionDecorator {
    constructor(version: IProduct) {
        super(version, 10, 'Dodatkowa gąbka do czyszczenia obuwia');
    }
}

export class Outlet extends OptionDecorator {
    constructor(version: IProduct) {
        super(version, -25, 'Ostatnie sztuki');
    }
}