export class Thing {

    constructor(private theString: string, ...dependencies: any[]) {
        console.log(theString, dependencies);
    }
}

export interface IThingFactory {
    getThing(theString: string): Thing;
}

export class ThingFactory implements IThingFactory {

    public getThing(theString: string): Thing  {
        return new Thing(theString, 'firstDependency', 'secondDependency');
    }
}

const factory: IThingFactory = new ThingFactory();
factory.getThing('Diabolic thing ]:->');
factory.getThing('Sweet thing ;)');
factory.getThing('Just a thing.');