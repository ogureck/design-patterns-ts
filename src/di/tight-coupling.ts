export class Traveler {
    public car: Car;

    constructor() {
        this.car = new Car; // thight coupling
    }

    public startJourney(): void {
        this.car.move();
    }
}

export class Car {
    public move(): void {
        console.log('Moving');
    }
}

let t = new Traveler();
t.startJourney();