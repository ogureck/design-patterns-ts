/*
    "Dependency Injection" is a 25-dollar term for a 5-cent concept. [...]
    Dependency injection means giving an object its instance variables. [...].
*/

interface Vehicle {
    move(): void;
}

export class Traveler {
    constructor(public vehicle: Vehicle) {
    }

    public startJourney(): void {
        this.vehicle.move();
    }
}

export class Car implements Vehicle {
    public move(): void {
        console.log('Car moving');
    }
}

export class Bike implements Vehicle {
    public move(): void {
        console.log('Bike moving');
    }
}

export class Truck extends Car {
    public move(): void {
        console.log('Truck moving');
    }
}

let travelers: Traveler[]  = [
    new Traveler(new Car()),
    new Traveler(new Bike()),
    new Traveler(new Truck()),
];
travelers.forEach(t => t.startJourney());