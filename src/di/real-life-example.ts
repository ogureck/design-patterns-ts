// mock NG
const Injectable = () => (...args: any[]) => {};
const Component = (metadata: any) => (...args: any[]) => {};


@Injectable()
export class HeroService {
    constructor() {
    }

    public getHeroes(): string[] {
        return [];
    }
}

@Component({
    // ...
})
export class HeroListComponent {

    public heroes: string[];

    // An Angular injector is responsible for creating service instances and injecting them into classes
    constructor(heroService: HeroService) {
        this.heroes = heroService.getHeroes();
    }
}