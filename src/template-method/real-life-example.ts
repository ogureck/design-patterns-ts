export abstract class BaseDigestThread {

    protected logger: any;
    public isStop: boolean;

    protected constructor(public id: string, public timeoutPeriod: number) {
    }

    public process(): void {
        while (!this.isStop) {
            let i = 0;
            while (!this.isStop && (i < this.timeoutPeriod)) {
                //... some logic here
            }
            this.innerProcess();
        }
    }

    protected abstract innerProcess(): void;

}

export class DigestProducerThread extends BaseDigestThread {

    public constructor(public id: string, public timeoutPeriod: number, protected digestMailBll: any) {
        super(id, timeoutPeriod)
    }

    protected innerProcess(): void {
        this.logger.Trace("Thread {0}. Process.", this.id);

        try {
            let res = this.digestMailBll.GetReadyToProcessItem();
            if (res != null && res.Length > 0) {
                res.focus(digestMailItem => {
                    this.logger.Trace("Processed Item id:{0}, objectId:{1}.", digestMailItem.Id, digestMailItem.ObjectId);
                });
            }
        } catch (exception) {
            this.logger.Error(exception, "Error Process Process Item.");
        }
    }
}

export class MaintenanceScheduleThread extends BaseDigestThread {

    public constructor(public id: string, public timeoutPeriod: number) {
        super(id, timeoutPeriod)
    }

    protected innerProcess(): void {
        this.logger.Trace("Maintenance Thread. Process.");
        this.processNewScheduleItems();
        this.processStuckScheduleItems();
        this.processCleanDigestScheduleItems();
        this.processUnscheduledDigestScheduleItems();
        this.pollectNewDigestMailItems();
        this.pollectStuckDigestMailItems();
    }

    /// logic hidden
    private processNewScheduleItems(): void {
    }

    private processStuckScheduleItems(): void {
    }

    private processCleanDigestScheduleItems(): void {
    }

    private processUnscheduledDigestScheduleItems(): void {
    }

    private pollectNewDigestMailItems(): void {
    }

    private pollectStuckDigestMailItems(): void {
    }
}