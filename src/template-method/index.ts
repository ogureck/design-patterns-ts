export abstract class Animal {

    public abstract makeSound(): void;

    public abstract eatFood(): void;

    public abstract sleep(): void;

    public doEveryday(): void {
        this.makeSound();
        this.eatFood();
        this.sleep();
    }
}

export class Dog extends Animal {

    public makeSound(): void {
        console.log('bark');
    }

    public eatFood(): void {
        console.log('eat dog food');
    }

    public sleep(): void {
        console.log('sleep a lot!');
    }
}

export class Cat extends Animal {

    public makeSound(): void {
        console.log('meow');
    }

    public eatFood(): void {
        console.log('eat cat food');
    }

    public sleep(): void {
        console.log('sleep a only during days!');
    }
}

let animals: Animal[] = [ new Dog(), new Cat() ];
animals.forEach(a => a.doEveryday());