export interface Logger {
    error(msg: any): void;

    info(msg: any): void;

    trace(msg: any): void;

    debug(msg: any): void;
}

export class LogManager {

    private static _instance: Logger = null;

    public static getCurrentClassLogger(): Logger {
        if (this._instance === null) {
            const log = (type, msg) => console.log(`${new Date().toLocaleString()}\t[${type}]\t${msg}`);

            this._instance = {
                error: m => log('ERROR', m),
                info: m => log('INFO', m),
                trace: m => log('TRACE', m),
                debug: m => log('DEBUG', m)
            };
        }
        return this._instance;
    }
}

export class ActiveMQClient {
    private static readonly Logger: Logger = LogManager.getCurrentClassLogger();

    constructor() {
        ActiveMQClient.Logger.debug('AMQ client created');
    }

    public connect(): void {
        try {
            ActiveMQClient.Logger.trace('Openning AMQ connection...');
            // ...
            ActiveMQClient.Logger.info('AMQ connection established');

            throw 'Oppsie! Something went wrong :('
        } catch (e) {
            ActiveMQClient.Logger.error(e);
        }

    }
}

let amq = new ActiveMQClient();
amq.connect();